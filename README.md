

&emsp;


### Stata Plus：连老师的 Stata 外部命令集

#### 项目介绍

- 内容：存放了自 2003 一年以来我下载的所有外部命令。
- 更新时间：`2020/2/8 17:06`
- 命令清单：[- 点击查看 -](https://gitee.com/arlionn/StataPlus/blob/master/lian_plus_tree.txt) 

#### 下载
- **地址1**：百度云盘  <https://pan.baidu.com/s/1Ea1l4nCrB3n7GZywA5PU2A> , 提取码：riom 
- **地址2**：坚果云 <https://www.jianguoyun.com/p/DR6o7KcQtKiFCBi7-tUC> , 速度快


#### 使用方法

下载 **plus.rar** 后，与你的 plus 文件夹合并或直接覆盖你的 plus 文件夹。

- **方法1：** 下载 [「plus.rar」](https://pan.baidu.com/s/1-nvAG8ZDihWcKakViug_QQ) 到本地，解压后，放置于 **D:\stata15\ado** 文件夹下即可。若有自建的 **plus** 文件夹，可以将二者合并，或者直接覆盖 (我的外部命令应该更全面一些)。
- **方法2：** 若想同时保留你自己的 **plus** 和我提供的 **plus** 文件夹，则可以将我的重命名为 **plus2**，然后在 **profile.do** （存放于 `D:\stata15` 目录下）添加如下语句：`adopath + "D:\stata15\ado\plus2"`(绝对路径)，或者 `adopath + "c(sysdir_stata)\ado\plus2"` (相对路径)。重启 Stata 后即可保证 **plus2** 中的命令生效。
- ps，使用过程中可能遇到的问题，都可以在这里找到解答：[[Stata: 外部命令的搜索、安装与使用]](https://zhuanlan.zhihu.com/p/48703028)

### 文件路径相关说明

> plus 文件夹的存放位置

输入 `sysdir` 可以查看你的 plus 文件夹存放于何处。我的文件路径如下：
```stata
. sysdir
   STATA:  D:\stata15\
    BASE:  D:\stata15\ado\base\
    SITE:  D:\stata15\ado\site\
    PLUS:  D:\stata15/ado\plus\
PERSONAL:  D:\stata15/ado\personal\
OLDPLACE:  c:\ado\
```

> Stata 能够识别的 ado 文件存放位置

输入 `adopath` 可以查看 Stata 能够识别的所有 ado 文件的存放位置。如下是我电脑中的设置：

```stata
. adopath
  [1]  (BASE)      "D:\stata15\ado\base/"
  [2]  (SITE)      "D:\stata15\ado\site/"
  [3]              "."
  [4]  (PERSONAL)  "D:\stata15/ado\personal/"
  [5]  (PLUS)      "D:\stata15/ado\plus/"
  [6]  (OLDPLACE)  "c:\ado/"
  [7]              "D:\stata15/\ado\personal\_myado"
```


&emsp;

&emsp;

----

> [连享会](https://gitee.com/arlionn)-直播平台上线了！大家可以随时随地充电了！             
> <http://lianxh.duanshu.com>            
<img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">



> ### 连享会-直播课


- **E.** &#x1F4D7; [直播-经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26)  （**New！**）    
   &emsp; &emsp; 嘉宾：李兵 (中山大学), 2020年2月28(周五)，19:00-21:00. [「课程详情」](https://www.lianxh.cn/news/761e6bbfe07a8.html)
- **D.** &#x1F535; [直播-空间计量全局模型及Matlab实现](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), **随时观看**，嘉宾：范巧。[「课程详情」](https://www.lianxh.cn/news/6fdb88905419e.html)
- **C.** &#x1F34E; [直播-动态面板数据模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e), **随时观看**，嘉宾：连玉君。[「课程详情」](https://www.lianxh.cn/news/594aa12c096ca.html)
- **B.** &#x1F34F; [直播-实证研究设计](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA)，**随时观看**，嘉宾：连玉君。[「课程详情」](https://www.lianxh.cn/news/2f31aa3347e83.html)     
- **A.** &#x1F36A; [直播-文本分析与爬虫专题](https://gitee.com/arlionn/Course/blob/master/Done/2020Text.md)，**2020.3.28-29， 4.4-5**，嘉宾：司继春, 游万海。[「课程详情」](https://www.lianxh.cn/news/88426b2faeea8.html)      
- **O.** &#x1F4D7; [公开课-直播-直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)，嘉宾：连玉君，**Free**. [「课程详情」](https://gitee.com/arlionn/PanelData) 



&emsp;

---
>#### 关于我们

>[连享会](https://www.lianxh.cn) &ensp; [最新专题](https://gitee.com/arlionn/Course/blob/master/README.md) &ensp;  [直播](http://lianxh.duanshu.com) 

![点我 - 更多推文](https://images.gitee.com/uploads/images/2020/0222/181340_968f61e0_1522177.png)

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- **欢迎赐稿：** 欢迎赐稿至StataChina@163.com。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **往期精彩推文：**
 [Stata绘图](https://mp.weixin.qq.com/s/xao8knOk0ulGfNc7vasfew) | [时间序列+面板数据](https://mp.weixin.qq.com/s/8yP1Dijylgreg59QIkqnMg) | [Stata资源](https://mp.weixin.qq.com/s/Kdeoi5uJyNtwwwptdQDQDQ) | [数据处理+程序](https://mp.weixin.qq.com/s/_3DQacFyy7juRjgFedp9WQ) |  [回归分析-交乘项-内生性](https://mp.weixin.qq.com/s/61qJNWnL4KRp0fbLxuDGww)
- **Stata连享会 (StataChina) 公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文。常见关键词：`DID，RDD，PSM，面板，IV，合成控制法，plus，Profile, Bootstrap, 交乘项, 平方项, 工具, 软件, Sai2, gInk, Annotator, 直击面板数据, 连老师, 直播, 空间计量, 爬虫, 文本分析, 正则, Markdown幻灯片, marp, 盈余管理, MC` ……。


---

![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2020/0222/181340_3ca42f9f_1522177.png)





